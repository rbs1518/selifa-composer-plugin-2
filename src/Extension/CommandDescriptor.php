<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Extension;

/**
 * Class CommandDescriptor
 *
 * @package RBS\Selifa\Composer\Framework
 */
class CommandDescriptor
{
    /**
     * @var string
     */
    public $Name = '';

    /**
     * @var string
     */
    public $Description = '';

    /**
     * @var CommandOption[]
     */
    public $Options = [];

    /**
     * @var CommandArgument[]
     */
    public $Arguments = [];

    /**
     * @var string[]
     */
    public $Usages = [];

    /**
     * @param string $name
     * @param int $mode
     * @param string $desc
     * @param null $default
     */
    public function AddArgument($name,$mode,$desc='',$default=null)
    {
        $arg = new CommandArgument();
        $arg->Name = $name;
        $arg->Mode = $mode;
        $arg->Description = $desc;
        $arg->Default = $default;
        $this->Arguments[] = $arg;
    }

    /**
     * @param string $name
     * @param int $mode
     * @param string $desc
     * @param null|mixed $default
     * @param null|string|string[] $shortcut
     */
    public function AddOption($name,$mode,$desc='',$default=null,$shortcut=null)
    {
        $opt = new CommandOption();
        $opt->Name = $name;
        $opt->Mode = $mode;
        $opt->Description = $desc;
        $opt->Default = $default;
        $opt->Shortcut = $shortcut;
        $this->Options[] = $opt;
    }
}
?>