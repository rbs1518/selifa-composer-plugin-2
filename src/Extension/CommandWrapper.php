<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Extension;
use Composer\Command\BaseCommand;
use Composer\IO\IOInterface;
use RBS\Selifa\Composer\Framework\Configuration;
use RBS\Selifa\Composer\Interfaces\IFrameworkCommand;
use RBS\Selifa\Composer\IO\CombinedIOWrapper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

/**
 * Class CommandWrapper
 *
 * @package RBS\Selifa\Composer\Extension
 */
class CommandWrapper extends BaseCommand
{
    /**
     * @var null|IFrameworkCommand
     */
    private $_FrameworkCommand = null;

    /**
     * @var null|IOInterface;
     */
    private $_ComposerIO = null;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $io = new CombinedIOWrapper($input,$output,$this->_ComposerIO);
        $this->_FrameworkCommand->Initialized($this,$io);
    }

    /**
     * @throws Exception
     */
    protected function configure()
    {
        $cDesc = $this->_FrameworkCommand->GetDescriptor();
        if ($cDesc === null)
            throw new Exception('No descriptor for command ['.get_class($this->_FrameworkCommand).'].');

        $this->setName($cDesc->Name);
        $this->setDescription($cDesc->Description);
        foreach ($cDesc->Options as $cOpt)
            $this->addOption($cOpt->Name,$cOpt->Shortcut,$cOpt->Mode,$cOpt->Description,$cOpt->Default);
        foreach ($cDesc->Arguments as $cArg)
            $this->addArgument($cArg->Name,$cArg->Mode,$cArg->Description,$cArg->Default);
        foreach ($cDesc->Usages as $usage)
            $this->addUsage($usage);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new CombinedIOWrapper($input,$output,$this->_ComposerIO);
        $this->_FrameworkCommand->Execute($io);
    }

    /**
     * CommandWrapper constructor.
     *
     * @param IFrameworkCommand $fCmd
     * @param IOInterface $io
     */
    public function __construct(IFrameworkCommand $fCmd,$io)
    {
        $this->_FrameworkCommand = $fCmd;
        $this->_ComposerIO = $io;
        parent::__construct(null);
    }

    /**
     * @return Configuration
     */
    public function GetConfiguration()
    {
        return Configuration::Instance();
    }
}
?>