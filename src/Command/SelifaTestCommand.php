<?php
namespace RBS\Selifa\Composer\Command;
use Composer\Command\BaseCommand;
use RBS\Selifa\Composer\IO\InputOutputWrapper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

/**
 * Class SelifaTestCommand
 * @package RBS\Selifa\Composer\Command
 */
class SelifaTestCommand extends BaseCommand
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function configure()
    {
        $this->setName('selifa-test');
        $this->setDescription('For testing purposes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new InputOutputWrapper($input,$output);

        $io->write('TEST INAI');

        $composer = $this->getComposer();
        $x = $composer->getPackage()->getExtra();
        var_dump($x);

    }
}
?>