<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Command;
use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use Composer\Command\BaseCommand;
use Exception;
use RBS\Selifa\Composer\Extension\CommandWrapper;
use RBS\Selifa\Composer\Framework\Configuration;
use RBS\Selifa\Composer\Framework\InstallData;
use RBS\Selifa\Composer\Interfaces\IFrameworkCommand;
use RBS\Selifa\Composer\Interfaces\IFrameworkCommandPackage;
use RBS\Selifa\Composer\SelifaPlugin;

/**
 * Class CommandProvider
 *
 * @package RBS\Selifa\Composer\Command
 */
class CommandProvider implements CommandProviderCapability
{
    /**
     * @var null|Composer
     */
    private $_Composer = null;

    /**
     * @var null|SelifaPlugin
     */
    private $_Plugin = null;

    /**
     * @var null|IOInterface;
     */
    private $_ComposerIO = null;

    /**
     * @var IFrameworkCommand[]
     */
    private $_CommandExtensions = [];

    /**
     * @var bool
     */
    private $_IsSelifaLoaded = false;

    /**
     *
     */
    protected function LoadSelifaFramework()
    {
        $coreOpts = [
            'RootPath' => SELIFA_ROOT_PATH,
            'DefaultConfigDir' => 'defaults',
            'ConfigDir' => 'configs',
            'EnvironmentVars' => 3,
            'UseComposer' => true,
            'LoadComponents' => [
                '\RBS\Selifa\XM' => [
                    'EnableTrace' => true,
                    'VerboseInternalException' => false,
                    'VerboseSystemException' => false,
                    'TraceExceptionTree' => false,
                    'HandleDefaultException' => false
                ]
            ]
        ];
        \RBS\Selifa\Core::Initialize($coreOpts);
        $this->_IsSelifaLoaded = true;
    }

    /**
     * @param Configuration $cfg
     * @throws Exception
     */
    protected function LoadFromPluginConfig($cfg)
    {
        $pConfig = $cfg->LoadLocalPluginConfigFile();
        if (isset($pConfig['command-packages']))
        {
            $cmdPackages = $pConfig['command-packages'];
            foreach ($cmdPackages as $packageCn)
            {
                $cmdPackage = new $packageCn();
                if (!($cmdPackage instanceof IFrameworkCommandPackage))
                    throw new Exception('['.$packageCn.'] does not implement IFrameworkCommandPackage.');
                foreach ($cmdPackage->GetCommands() as $fCmd)
                    $this->_CommandExtensions[] = $fCmd;
            }
        }
    }

    /**
     * @param InstallData $iData
     * @throws Exception
     */
    protected function LoadFromInstallData($iData)
    {
        $cmdPackages = $iData->GetAllCommandPackages();
        foreach ($cmdPackages as $packageCn)
        {
            $cmdPackage = new $packageCn();
            if (!($cmdPackage instanceof IFrameworkCommandPackage))
                throw new Exception('['.$packageCn.'] does not implement IFrameworkCommandPackage.');
            foreach ($cmdPackage->GetCommands() as $fCmd)
                $this->_CommandExtensions[] = $fCmd;
        }
    }

    /**
     * CommandProvider constructor.
     *
     * @param array $ctorArgs
     * @throws Exception
     */
    public function __construct($ctorArgs)
    {
        if (!isset($ctorArgs['composer']))
            throw new Exception('Missing composer object in constructor arguments.');
        $this->_Composer = $ctorArgs['composer'];

        if (!isset($ctorArgs['io']))
            throw new Exception('Missing composer io object in constructor arguments.');
        $this->_ComposerIO = $ctorArgs['io'];

        if (!isset($ctorArgs['plugin']))
            throw new Exception('Missing plugin object in constructor arguments.');
        $this->_Plugin = $ctorArgs['plugin'];

        $extra = $this->_Composer->getPackage()->getExtra();

        $cfg = Configuration::Instance();
        $sInitPath = ('init.php');
        if (isset($extra['selifa']['init-path']))
            $sInitPath = trim($extra['selifa']['init-path']);
        $sInitPath = ($cfg->RootDir.$sInitPath);
        if (file_exists($sInitPath))
        {
            include($sInitPath);
            if (!defined('SELIFA'))
                throw new Exception('Selifa initialization found, but no signature detected.');
            $this->LoadSelifaFramework();
        }

        if ($this->_IsSelifaLoaded)
        {
            if (isset($extra['selifa']['command-package-source']))
            {
                $cmdPkgSrc = strtolower(trim($extra['selifa']['command-package-source']));
                if ($cmdPkgSrc == 'plugin-config')
                    $this->LoadFromPluginConfig($cfg);
            }

            $this->LoadFromInstallData($this->_Plugin->GetInstallData());
        }
    }

    #region CommandProviderCapability implementation
    /**
     * @return BaseCommand[]
     */
    public function getCommands()
    {
        $cmds = [
            new SelifaProjectInitCommand(),
            new SelifaUpdateCoreCommand(),
            new SelifaTestCommand()
        ];

        if (($this->_IsSelifaLoaded) && (count($this->_CommandExtensions) > 0))
        {
            foreach ($this->_CommandExtensions as $cmdExt)
                $cmds[] = new CommandWrapper($cmdExt,$this->_ComposerIO);
        }

        return $cmds;
    }
    #endregion
}
?>