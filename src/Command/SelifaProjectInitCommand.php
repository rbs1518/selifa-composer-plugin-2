<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Command;
use RBS\Selifa\Composer\IO\BitBucketCoreDownloader;
use RBS\Selifa\Composer\IO\FileGenerator;
use RBS\Selifa\Composer\IO\FileManager;
use RBS\Selifa\Composer\IO\InputOutputWrapper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

/**
 * Class SelifaProjectInitCommand
 *
 * @package RBS\Selifa\Composer\Command
 */
class SelifaProjectInitCommand extends BaseSelifaCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this->setName('selifa-init-project');
        $this->setDescription('Initialize project based on selifa framework.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new InputOutputWrapper($input,$output);
        $output->write('Initializing new project...',true);

        $downloader = new BitBucketCoreDownloader($io);
        $bData = $downloader->Download('rbs1518','selifa-core');

        $output->write("\tSelifa Core Version: ".$bData['Version'],true);
        FileManager::Instance()->DeployCore($bData['Binary'],$bData['Version']);

        FileGenerator::Instance()->GenerateDevelopmentBootstraps();
        $output->write('New project initialized.',true);
    }
}
?>