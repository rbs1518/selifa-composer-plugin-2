<?php
namespace RBS\Selifa\Composer\Command;
use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

/**
 * Class BaseSelifaCommand
 * @package RBS\Selifa\Composer\Command
 */
abstract class BaseSelifaCommand extends BaseCommand
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

    }
}
?>