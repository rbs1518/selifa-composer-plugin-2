<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\IO;
use RBS\Selifa\Composer\Interfaces\ICoreDownloader;
use RBS\Selifa\Composer\Interfaces\IConsoleIOWrapper;
use Exception;

/**
 * Class BitBucketCoreDownloader
 *
 * @package RBS\Selifa\Composer\IO
 */
class BitBucketCoreDownloader implements ICoreDownloader
{
    /**
     * @var null|IConsoleIOWrapper
     */
    protected $_IO = null;

    /**
     * @param IConsoleIOWrapper $io
     */
    public function __construct(IConsoleIOWrapper $io)
    {
        $this->_IO = $io;
    }

    private function _OnDownloadProgress($resource,$download_size,$downloaded,$upload_size,$uploaded)
    {
        /*if($download_size > 0)
        {
            $pct = ($downloaded / $download_size  * 100);
            echo ("\r                                                          ");
            usleep(10);
            echo ("\r".'['.$downloaded.'/'.$download_size.'] ['.$pct.']');
            usleep(10);
        }*/
    }

    /**
     * @param string $workspace
     * @param string $repo
     * @return array
     * @throws Exception
     */
    protected function GetLatestTagFromBitbucket($workspace,$repo)
    {
        $url = "https://api.bitbucket.org/2.0/repositories/".$workspace."/".$repo."/refs/tags?sort=-name";
        $ch = curl_init();
        $opt = [
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
        ];
        curl_setopt_array($ch, $opt);
        $output = curl_exec($ch);
        $error = trim(curl_error($ch));
        curl_close($ch);

        if ($error != '')
            throw new Exception('API response error: '.$error);

        $data = json_decode($output,true);
        if (!isset($data['values'][0]))
            throw new Exception('API response error: invalid response format.');

        $item = $data['values'][0];
        return $item;
    }

    /**
     * @param string $workspace
     * @param string $repo
     * @return array
     * @throws Exception
     */
    public function Download($workspace,$repo)
    {
        try
        {
            $this->_IO->write("\tRetrieving refs from Bitbucket... ",false);
            $tagInfo = $this->GetLatestTagFromBitbucket($workspace,$repo);
            $tag = trim($tagInfo['name']);
            $this->_IO->write('[<fg=green>OK</>]',true);

            $url = "https://bitbucket.org/".$workspace."/".$repo."/get/".$tag.".zip";
            $ch = curl_init();
            $opt = [
                CURLOPT_URL => $url,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_BINARYTRANSFER => true,
                CURLOPT_HEADER => false
                //CURLOPT_PROGRESSFUNCTION => 'download_progress',
                //CURLOPT_NOPROGRESS => false
            ];
            curl_setopt_array($ch, $opt);

            $this->_IO->write("\tRetrieving latest version from Bitbucket... ",false);
            $output = curl_exec($ch);
            $error = trim(curl_error($ch));
            curl_close($ch);
            $this->_IO->write('[<fg=green>OK</>]',true);

            if ($error != '')
                throw new Exception('Fetch error: '.$error);

            return [
                'Tag' => $tag,
                'Version' => $tag,
                'Binary' => $output
            ];
        }
        catch (Exception $x)
        {
            $msg = ('Core retrieval error: '.$x->getMessage());
            $this->_IO->write('[<fg=red>FAILED</>] '.$msg);
            return null;
        }
    }
}
?>