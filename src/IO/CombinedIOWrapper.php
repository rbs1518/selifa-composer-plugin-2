<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\IO;
use RBS\Selifa\Composer\Interfaces\IConsoleIOWrapper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\IO\IOInterface;
use InvalidArgumentException;

/**
 * Class CombinedIOWrapper
 * @package RBS\Selifa\Composer\IO
 */
class CombinedIOWrapper implements IConsoleIOWrapper
{
    /**
     * @var null|InputInterface
     */
    private $_Input = null;

    /**
     * @var null|OutputInterface
     */
    private $_Output = null;

    /**
     * @var null|IOInterface
     */
    private $_IO = null;

    /**
     * CombinedIOWrapper constructor.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param IOInterface $io
     */
    public function __construct(InputInterface $input, OutputInterface $output, IOInterface $io)
    {
        $this->_Input = $input;
        $this->_Output = $output;
        $this->_IO = $io;
    }

    /**
     * Is this input means interactive?
     *
     * @return bool
     */
    public function isInteractive()
    {
        return $this->_Input->isInteractive();
    }

    /**
     * Is this output verbose?
     *
     * @return bool
     */
    public function isVerbose()
    {
        return $this->_Output->isVerbose();
    }

    /**
     * Is the output very verbose?
     *
     * @return bool
     */
    public function isVeryVerbose()
    {
        return $this->_Output->isVeryVerbose();
    }

    /**
     * Is the output in debug verbosity?
     *
     * @return bool
     */
    public function isDebug()
    {
        return $this->_Output->isDebug();
    }

    /**
     * Is this output decorated?
     *
     * @return bool
     */
    public function isDecorated()
    {
        return $this->_Output->isDecorated();
    }

    /**
     * Writes a message to the output.
     *
     * @param string|array $messages  The message as an array of lines or a single string
     * @param bool         $newline   Whether to add a newline or not
     */
    public function write($messages, $newline = true)
    {
        $this->_Output->write($messages,$newline);
    }

    /**
     * Asks a question to the user.
     *
     * @param string $question The question to ask
     * @param string $default  The default answer if none is given by the user
     *
     * @throws \RuntimeException If there is no data to read in the input stream
     * @return string|null       The user answer
     */
    public function ask($question, $default = null)
    {
        return $this->_IO->ask($question,$default);
    }

    /**
     * Asks a confirmation to the user.
     *
     * The question will be asked until the user answers by nothing, yes, or no.
     *
     * @param string $question The question to ask
     * @param bool   $default  The default answer if the user enters nothing
     *
     * @return bool true if the user has confirmed, false otherwise
     */
    public function askConfirmation($question, $default = true)
    {
        return $this->_IO->askConfirmation($question,$default);
    }

    /**
     * Asks for a value and validates the response.
     *
     * The validator receives the data to validate. It must return the
     * validated data when the data is valid and throw an exception
     * otherwise.
     *
     * @param string   $question  The question to ask
     * @param callable $validator A PHP callback
     * @param null|int $attempts  Max number of times to ask before giving up (default of null means infinite)
     * @param mixed    $default   The default answer if none is given by the user
     *
     * @throws \Exception When any of the validators return an error
     * @return mixed
     */
    public function askAndValidate($question, $validator, $attempts = null, $default = null)
    {
        return $this->_IO->askAndValidate($question,$validator,$attempts,$default);
    }

    /**
     * Asks a question to the user and hide the answer.
     *
     * @param string $question The question to ask
     *
     * @return string|null The answer
     */
    public function askAndHideAnswer($question)
    {
        return $this->_IO->askAndHideAnswer($question);
    }

    /**
     * Asks the user to select a value.
     *
     * @param string      $question     The question to ask
     * @param array       $choices      List of choices to pick from
     * @param bool|string $default      The default answer if the user enters nothing
     * @param bool|int    $attempts     Max number of times to ask before giving up (false by default, which means infinite)
     * @param string      $errorMessage Message which will be shown if invalid value from choice list would be picked
     * @param bool        $multiselect  Select more than one value separated by comma
     *
     * @throws \InvalidArgumentException
     * @return int|string|array|bool     The selected value or values (the key of the choices array)
     */
    public function select($question, $choices, $default, $attempts = false, $errorMessage = 'Value "%s" is invalid', $multiselect = false)
    {
        return $this->_IO->select($question,$choices,$default,$attempts,$errorMessage,$multiselect);
    }

    /**
     * Returns all the given arguments merged with the default values.
     *
     * @return array<string|bool|int|float|array|null>
     */
    public function getArguments()
    {
        return $this->_Input->getArguments();
    }

    /**
     * Returns the argument value for a given argument name.
     *
     * @param string $name
     * @return mixed
     * @throws InvalidArgumentException When argument given doesn't exist
     */
    public function getArgument($name)
    {
        return $this->_Input->getArgument($name);
    }

    /**
     * Returns true if an InputArgument object exists by name or position.
     *
     * @param string $name
     * @return bool true if the InputArgument object exists, false otherwise
     */
    public function hasArgument($name)
    {
        return $this->_Input->hasArgument($name);
    }

    /**
     * Returns all the given options merged with the default values.
     *
     * @return array<string|bool|int|float|array|null>
     */
    public function getOptions()
    {
        return $this->_Input->getOptions();
    }

    /**
     * Returns the option value for a given option name.
     *
     * @param string $name
     * @return mixed
     *
     * @throws InvalidArgumentException When option given doesn't exist
     */
    public function getOption($name)
    {
        return $this->_Input->getOption($name);
    }

    /**
     * Returns true if an InputOption object exists by name.
     *
     * @param string $name
     * @return bool true if the InputOption object exists, false otherwise
     */
    public function hasOption($name)
    {
        return $this->_Input->hasOption($name);
    }
}
?>