<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\IO;
use Composer\IO\IOInterface;
use DateTime;
use RBS\Selifa\Composer\Framework\Configuration;
use RBS\Selifa\Composer\Framework\InstallData;
use ZipArchive;
use Exception;

/**
 * Class FileManager
 * @package RBS\Selifa\Composer
 */
class FileManager
{
    /**
     * @var null|FileManager
     */
    protected static $_Instance = null;

    /**
     * @var null|IOInterface
     */
    protected $IO = null;

    /**
     * @var string
     */
    protected $RootDir = '';

    /**
     * @var string
     */
    protected $TempDir = '';

    /**
     * @var string
     */
    protected $DevDir = '';

    /**
     * @param IOInterface $io
     * @param Configuration $config
     * @return null|FileManager
     */
    public static function Initialize(IOInterface $io, Configuration $config)
    {
        if (self::$_Instance == null)
            self::$_Instance = new FileManager($io,$config);
        return self::$_Instance;
    }

    /**
     * @return static
     */
    public static function Instance()
    {
        return self::$_Instance;
    }

    /**
     * @param IOInterface $io
     * @param Configuration $config
     */
    private function __construct(IOInterface $io, Configuration $config)
    {
        $this->IO = $io;
        $this->RootDir = $config->RootDir;
        $this->TempDir = $config->TempDir;
        $this->DevDir = $config->DevDir;
    }

    /**
     * @param string $path
     * @return bool|null
     */
    public function IsDirectoryEmpty($path)
    {
        if (!is_readable($path))
            return null;
        $handle = opendir($path);
        while (false !== ($entry = readdir($handle)))
        {
            if (($entry != ".") && ($entry != ".."))
                return false;
        }
        return true;
    }

    /**
     * @param string $path
     * @param array $array
     * @param array $excludes
     */
    public function ParseDirectoryLevels($path,&$array,$excludes=array())
    {
        $nextDN = dirname($path);
        if (($nextDN == '.') || ($nextDN == ''))
            return;
        if (in_array($nextDN.DIRECTORY_SEPARATOR,$excludes))
            return;
        else
        {
            if (!in_array($nextDN,$array))
            {
                $array[] = $nextDN;
                $this->ParseDirectoryLevels($nextDN,$array,$excludes);
            }
            return;
        }
    }

    /**
     * @param array $source
     * @param array $excludes
     * @return array
     */
    public function GetUniqueDirectoryNameForAllLevels($source,$excludes=array())
    {
        $result = [];
        foreach ($source as $item)
        {
            $dn = dirname($item);
            if (!in_array($dn, $result))
            {
                $result[] = $dn;
                $this->ParseDirectoryLevels($dn,$result,$excludes);
            }
        }
        rsort($result,SORT_STRING);
        return $result;
    }

    /**
     * @param string $baseDir
     * @param string $dir
     * @param array $array
     */
    public function EnumerateDirectory($baseDir,$dir,&$array)
    {
        if ($dir == '')
            $cdir = scandir($baseDir);
        else
            $cdir = scandir($baseDir.DIRECTORY_SEPARATOR.$dir);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value,[".",".."]))
            {
                $fullPath = ($baseDir.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.$value);
                if (is_dir($fullPath))
                {
                    if ($dir == '')
                        $nDir = ($value);
                    else
                        $nDir = ($dir.DIRECTORY_SEPARATOR.$value);
                    $this->EnumerateDirectory($baseDir,$nDir,$array);
                }
                else
                {
                    if ($dir == '')
                        $array[] = $value;
                    else
                        $array[] = ($dir.DIRECTORY_SEPARATOR.$value);
                }
            }
        }
    }

    /**
     * @param string $dir
     * @return bool
     */
    public function DeleteDirectory($dir)
    {
        if (is_dir($dir))
            $dir_handle = opendir($dir);
        if (!$dir_handle)
            return false;
        while($file = readdir($dir_handle))
        {
            if ($file != "." && $file != "..")
            {
                if (!is_dir($dir.DIRECTORY_SEPARATOR.$file))
                    unlink($dir.DIRECTORY_SEPARATOR.$file);
                else
                    $this->DeleteDirectory($dir.DIRECTORY_SEPARATOR.$file);
            }
        }
        closedir($dir_handle);
        rmdir($dir);
        return true;
    }

    /**
     * @param string $srcDir
     * @param string $tgtDir
     * @param string $file
     */
    public function Copy($srcDir,$tgtDir,$file)
    {
        $f_SrcPath = ($srcDir.$file);
        $f_TgtPath = ($tgtDir.$file);
        $f_TgtPo = pathinfo($f_TgtPath,PATHINFO_DIRNAME);
        if (!file_exists($f_TgtPo))
            mkdir($f_TgtPo,0777,true);
        copy($f_SrcPath,$f_TgtPath);
    }

    /**
     * @param string $zipBinary
     * @param string $targetPath
     * @return string
     * @throws Exception
     */
    public function ExtractZipFromBinary($zipBinary,&$targetPath='')
    {
        if (!file_exists($this->TempDir))
            mkdir($this->TempDir,0777,true);
        $fnRand = hash('crc32',(new DateTime())->format('YmdHis').$zipBinary);

        if ($targetPath == '')
            $targetPath = ($this->TempDir.$fnRand.DIRECTORY_SEPARATOR);
        $tempZip = ($this->TempDir.'xf_'.$fnRand.'.zip');

        if (!file_exists($targetPath))
            mkdir($targetPath,0777,true);
        file_put_contents($tempZip,$zipBinary);

        $zip = new ZipArchive();
        if (!$zip->open($tempZip))
            throw new Exception('Could not open zip file ['.$tempZip.'].');

        $fPath = trim($zip->getNameIndex(0));
        $zip->extractTo($targetPath);
        $zip->close();
        unlink($tempZip);
        return $fPath;
    }

    /**
     * @param string $zipFile
     * @param string $targetPath
     * @return string
     * @throws Exception
     */
    public function ExtractZipFromFile($zipFile,$targetPath)
    {
        if (!file_exists($targetPath))
            mkdir($targetPath,0777,true);

        $zip = new ZipArchive();
        if (!$zip->open($zipFile))
            throw new Exception('Could not open zip file ['.$zipFile.'].');

        $fPath = trim($zip->getNameIndex(0));
        $zip->extractTo($targetPath);
        $zip->close();
        return $fPath;
    }

    /**
     * @param string $zipBinary
     * @param string $tagVersion
     * @throws Exception
     */
    public function DeployCore($zipBinary,$tagVersion)
    {
        if (!file_exists($this->TempDir))
            mkdir($this->TempDir,0777,true);

        $enableWrite = true;

        $fnRand = hash('crc32',(new DateTime())->format('YmdHis').$zipBinary);

        $tgtPath = $this->RootDir;
        $tempPath = ($this->TempDir.$fnRand.DIRECTORY_SEPARATOR);
        $tempZip = ($this->TempDir.'xf_'.$fnRand.'.zip');

        if (!file_exists($tgtPath))
            mkdir($tgtPath,0777,true);
        if (!file_exists($tempPath))
            mkdir($tempPath,0777,true);
        file_put_contents($tempZip,$zipBinary);

        $zip = new ZipArchive();
        if (!$zip->open($tempZip))
            throw new Exception('Could not open zip file ['.$tempZip.'].');

        $fPath = trim($zip->getNameIndex(0));
        $zip->extractTo($tempPath);
        $zip->close();
        unlink($tempZip);

        $pcFile = ($tempPath.$fPath.'_plugin_config.php');
        if (!file_exists($pcFile))
            throw new Exception('Plugin config file does not exists. Please check source SVN.');

        $pConfig = include($pcFile);

        $ifContent = '';
        if (isset($pConfig['InitContent']))
        {
            $icFile = ($tempPath.$fPath.trim($pConfig['InitContent']));
            if (file_exists($icFile))
                $ifContent = file_get_contents($icFile);
        }

        if (!isset($pConfig['SelifaVersion']))
            throw new Exception('SelifaVersion key does not exists.');
        $selifaVersion = trim($pConfig['SelifaVersion']);

        $selifaName = 'SELIFA';
        if (!isset($pConfig['SelifaName']))
            $selifaName = trim($pConfig['SelifaName']);

        if (!isset($pConfig['CoreInclude']))
            throw new Exception('CoreInclude key does not exists.');
        $coreInclude = trim($pConfig['CoreInclude']);

        $files = [];
        $this->EnumerateDirectory($tempPath.$fPath,'',$files);
        foreach ($files as $nFile)
        {
            $nDir = trim(pathinfo($nFile,PATHINFO_DIRNAME));
            if ($nDir == '.')
                $nDir = '';

            $ndParts = explode(DIRECTORY_SEPARATOR,$nFile);
            if (strtolower(trim($ndParts[0])) == 'tests')
                continue;

            $nTargetDir = ($tgtPath.$nDir);
            if ($enableWrite)
            {
                if (!file_exists($nTargetDir))
                    mkdir($nTargetDir,0777,true);
            }

            $srcFile = ($tempPath.$fPath.$nFile);
            $tgtFile = ($tgtPath.$nFile);

            if ($enableWrite)
            {
                if (($nDir == '') && ($nFile == 'LICENSE'))
                    copy($srcFile,$tgtFile);
                else if ($nDir != '')
                    copy($srcFile,$tgtFile);
            }
        }

        $coreInclude = (DIRECTORY_SEPARATOR.str_replace('/',DIRECTORY_SEPARATOR,$coreInclude));
        $fInitCnt  = "<?php\ndefine('SELIFA_ROOT_PATH',dirname(__FILE__));\n";
        $fInitCnt .= "define('SELIFA_TIME_STARTED',microtime(true));\n";
        $fInitCnt .= "define('SELIFA','v".$selifaVersion."');\n";
        $fInitCnt .= "define('SELIFA_NAME','".$selifaName."');\n";
        $fInitCnt .= "include(SELIFA_ROOT_PATH.'".$coreInclude."');\n\n";
        $fInitCnt .= $ifContent;
        $fInitCnt .= "\n?>";

        if ($enableWrite)
            file_put_contents($tgtPath.'init.php',$fInitCnt);
        $this->DeleteDirectory($tempPath);
    }

    /**
     * @param InstallData $iData
     * @param string $pName
     * @param string $sPath
     * @param array $extra
     * @throws Exception
     */
    public function InstallPackageFiles(InstallData $iData,$pName,$sPath,$extra)
    {
        $zFilePath = ($sPath.'temp.zip');
        if (!file_exists($zFilePath))
            throw new Exception('Downloaded file does not exists.');

        $actPath = $this->ExtractZipFromFile($zFilePath,$sPath);
        unlink($zFilePath);

        $iPath = ($sPath.$actPath);

        $pConfigs = [];
        $pcFilePath = ($iPath.'_plugin_config.php');
        if (file_exists($pcFilePath))
            $pConfigs = include($pcFilePath);

        if (isset($extra['source-path']))
            $sourceDir = ($iPath.trim($extra['source-path']).DIRECTORY_SEPARATOR);
        else if (isset($pConfigs['source_path']))
            $sourceDir = ($iPath.trim($pConfigs['source_path']).DIRECTORY_SEPARATOR);
        else
            $sourceDir = ($iPath.'src'.DIRECTORY_SEPARATOR);
        $cfgDir = ($sourceDir.'configs'.DIRECTORY_SEPARATOR);

        $files = [];
        $this->EnumerateDirectory($sourceDir,'',$files);
        foreach ($files as $file)
        {
            $fParts = explode(DIRECTORY_SEPARATOR,$file);
            if (strtolower(trim($fParts[0])) == 'configs')
                continue;

            $this->Copy($sourceDir,$this->RootDir,$file);
            $iData->AddPackageFile($pName,$file);
        }

        $configs = [];
        if (file_exists($cfgDir))
        {
            $cfgFiles = [];
            $this->EnumerateDirectory($cfgDir,'',$cfgFiles);
            foreach ($cfgFiles as $f_cfg)
            {
                $f_cfg_path = ($cfgDir.$f_cfg);
                if (strtolower(trim(pathinfo($f_cfg_path,PATHINFO_EXTENSION))) == 'php')
                {
                    $cfgName = strtolower(trim(pathinfo($f_cfg_path,PATHINFO_FILENAME)));
                    $configs[$cfgName] = include($f_cfg_path);
                }
            }
        }

        if (isset($pConfigs['configs']))
            $configs = array_replace_recursive($configs,$pConfigs['configs']);
        if (count($configs) > 0)
            $iData->SetPackageConfiguration($pName,$configs);
        if (isset($pConfigs['command-packages']))
            $iData->SetCommandPackages($pName,$pConfigs['command-packages']);
    }

    /**
     * @param array $files
     */
    public function RemoveInstalledPackageFiles($files)
    {
        $dirUniques = $this->GetUniqueDirectoryNameForAllLevels($files,[]);
        foreach ($files as $item)
        {
            $fn = ($this->RootDir.$item);
            if (file_exists($fn))
                unlink($fn);
        }
        foreach ($dirUniques as $item)
        {
            $fn = ($this->RootDir.$item);
            if (file_exists($fn))
            {
                if ($this->IsDirectoryEmpty($fn))
                    rmdir($fn);
            }
        }
    }
}
?>