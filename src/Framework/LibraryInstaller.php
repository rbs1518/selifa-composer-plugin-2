<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Framework;
use Composer\Composer;
use Composer\Downloader\DownloadManager;
use Composer\Installer\InstallerInterface;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use RBS\Selifa\Composer\IO\FileGenerator;
use RBS\Selifa\Composer\IO\FileManager;
use React\Promise\PromiseInterface;
use Exception;

/**
 * Class LibraryInstaller
 *
 * @package RBS\Selifa\Composer\Framework
 */
class LibraryInstaller implements InstallerInterface
{
    /**
     * @var null|Configuration
     */
    private $_Config = null;

    /**
     * @var null|IOInterface
     */
    private $_IO = null;

    /**
     * @var null|DownloadManager
     */
    private $_DM = null;

    /**
     * @var null|InstallData
     */
    private $_InstallData = null;

    /**
     * @var null|FileManager
     */
    private $_FM = null;

    /**
     * @var null|FileGenerator
     */
    private $_FG = null;

    /**
     * @param string $downloadUrl
     * @param string $downloadPath
     * @throws Exception
     */
    protected function DoDownload($downloadUrl,$downloadPath)
    {
        $ch = curl_init();
        $opt = [
            CURLOPT_URL => $downloadUrl,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_HEADER => false
        ];
        curl_setopt_array($ch, $opt);

        $output = curl_exec($ch);
        $error = trim(curl_error($ch));
        curl_close($ch);

        if ($error != '')
            throw new Exception('Download error: '.$error);

        $saveFile = ($downloadPath.'temp.zip');
        file_put_contents($saveFile,$output);
    }

    /**
     * LibraryInstaller constructor.
     *
     * @param Composer $composer
     * @param Configuration $config
     * @param IOInterface $io
     * @param InstallData $iData
     * @param FileManager $fManager
     * @param FileGenerator $fGenerator
     */
    public function __construct(
        Composer $composer,
        Configuration $config,
        IOInterface $io,
        InstallData $iData,
        FileManager $fManager,
        FileGenerator $fGenerator)
    {
        $this->_Config = $config;
        $this->_InstallData = $iData;
        $this->_DM = $composer->getDownloadManager();
        $this->_IO = $io;
        $this->_FM = $fManager;
        $this->_FG = $fGenerator;
    }

    /**
     * @param string $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        return ($packageType == 'selifa-library');
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     * @return bool
     */
    public function isInstalled(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $pName = $package->getPrettyName();
        return ($repo->hasPackage($package) && $this->_InstallData->IsPackageExists($pName));
    }

    /**
     * @param PackageInterface $package
     * @param PackageInterface|null $prevPackage
     * @return null|PromiseInterface
     * @throws Exception
     */
    public function download(PackageInterface $package, PackageInterface $prevPackage = null)
    {
        //TODO MUST IMPLEMENT SYMLINK AND FILE COPY STRATEGY

        $pName = $package->getPrettyName();
        $downloadPath = ($this->getInstallPath($package).DIRECTORY_SEPARATOR);
        $iSource = $package->getInstallationSource();
        if ($iSource !== null)
        {
            if ($iSource != 'dist')
                throw new Exception('Selifa library installer only works with dist type.');
        }

        $distUrls = $package->getDistUrls();
        if (count($distUrls) <= 0)
            throw new Exception('Unspecified dist url(s).');

        $downloadUrl = $distUrls[0];
        $this->_IO->write('  - Downloading <fg=cyan>'.$pName.'</> [<fg=yellow>'.$package->getPrettyVersion().'</>] ',false);
        $this->DoDownload($downloadUrl,$downloadPath);
        $this->_IO->write(' [<fg=green>OK</>]',true);

        return \React\Promise\resolve();
    }

    /**
     * @param string $type
     * @param PackageInterface $package
     * @param PackageInterface|null $prevPackage
     * @return null|PromiseInterface
     */
    public function prepare($type, PackageInterface $package, PackageInterface $prevPackage = null)
    {
        return \React\Promise\resolve();
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     * @return null|PromiseInterface
     * @throws Exception
     */
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $path = ($this->getInstallPath($package).DIRECTORY_SEPARATOR);
        $pName = $package->getPrettyName();
        $this->_InstallData->AddPackageFromInterface($package);

        $this->_IO->write('  - Installing <fg=cyan>'.$pName.'</> [<fg=yellow>'.$package->getPrettyVersion().'</>].',false);
        $this->_FM->InstallPackageFiles($this->_InstallData,$pName,$path,$package->getExtra());
        $this->_IO->write(' [<fg=green>OK</>]',true);

        if (!$repo->hasPackage($package))
            $repo->addPackage(clone $package);

        return \React\Promise\resolve();
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $initial
     * @param PackageInterface $target
     * @return null|PromiseInterface
     * @throws Exception
     */
    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        $path = ($this->getInstallPath($initial).DIRECTORY_SEPARATOR);
        $pName = $initial->getPrettyName();

        $zFilePath = ($path.'temp.zip');
        if (!file_exists($zFilePath))
            throw new Exception('Downloaded file does not exists.');

        if (!$this->_InstallData->IsPackageExists($pName))
            throw new Exception('Selifa library ['.$pName.'] does not exists in install data.');
        $pData = $this->_InstallData->GetPackage($pName);

        $this->_IO->write('  - Removing <fg=cyan>'.$pName.'</> [<fg=yellow>'.$initial->getPrettyVersion().'</>].',false);
        $this->_FM->RemoveInstalledPackageFiles($pData['files']);
        $this->_InstallData->ResetPackageFiles($pName);
        $this->_InstallData->ResetPackageConfigurations($pName);
        $this->_InstallData->ResetCommandPackages($pName);
        $this->_IO->write(' [<fg=green>OK</>]',true);

        $this->_IO->write('  - Installing <fg=cyan>'.$pName.'</> [<fg=yellow>'.$target->getPrettyVersion().'</>].',false);
        $this->_FM->InstallPackageFiles($this->_InstallData,$pName,$path,$target->getExtra());
        $this->_IO->write(' [<fg=green>OK</>]',true);

        $repo->removePackage($initial);
        if (!$repo->hasPackage($target))
            $repo->addPackage(clone $target);

        return \React\Promise\resolve();
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     * @return null|PromiseInterface
     * @throws Exception
     */
    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        $pName = $package->getPrettyName();

        if (!$this->_InstallData->IsPackageExists($pName))
            throw new Exception('Selifa library ['.$pName.'] does not exists in install data.');
        $pData = $this->_InstallData->GetPackage($pName);

        try
        {
            $this->_IO->write('  - Removing <fg=cyan>'.$pName.'</> [<fg=yellow>'.$package->getPrettyVersion().'</>].',false);
            $this->_FM->RemoveInstalledPackageFiles($pData['files']);
            $this->_InstallData->RemovePackage($pName);
            if (!$repo->hasPackage($package))
                $repo->removePackage($package);

            $this->_IO->write(' [<fg=green>OK</>]',true);
        }
        catch (Exception $x)
        {
            $this->_IO->write("\t<fg=red>[ ".$x->getMessage()." ]</>",true);
        }

        return \React\Promise\resolve();
    }

    /**
     * @param string $type
     * @param PackageInterface $package
     * @param PackageInterface|null $prevPackage
     * @return null|PromiseInterface
     */
    public function cleanup($type, PackageInterface $package, PackageInterface $prevPackage = null)
    {
        $pName = $package->getPrettyName();
        if ($this->_InstallData->IsPackageExists($pName))
            $this->_InstallData->RemovePackage($pName);
    }

    /**
     * @param PackageInterface $package
     * @return string
     */
    public function getInstallPath(PackageInterface $package)
    {
        $path  = ($this->_Config->TempDir.'composer-installs'.DIRECTORY_SEPARATOR);
        $path .= ($package->getPrettyName());
        if (!file_exists($path))
            mkdir($path,0777,true);
        return $path;
    }
}
?>