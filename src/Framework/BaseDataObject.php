<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Framework;

/**
 * Class BaseDataObject
 *
 * @package RBS\Selifa\Composer\Framework
 */
abstract class BaseDataObject
{
    /**
     * @var string
     */
    protected $WorkingDir = '';

    /**
     * @var string
     */
    protected $JsonFile = '';

    /**
     * @var null|array
     */
    protected $Data = null;

    /**
     * BaseDataObject constructor.
     * @param Configuration $config
     */
    public function __construct(Configuration $config)
    {
        $this->WorkingDir = $config->RootDir;
    }

    /**
     * @param string $dataFile
     */
    protected function SetDataFileName($dataFile)
    {
        $this->JsonFile = ($this->WorkingDir.$dataFile);
        if (file_exists($this->JsonFile))
            $this->Data = json_decode(file_get_contents($this->JsonFile),true);
        else
            $this->InitializeIfFileNotExists($this->JsonFile);
    }

    /**
     *
     */
    public function WriteToFile()
    {
        $content = json_encode($this->Data,JSON_PRETTY_PRINT);
        file_put_contents($this->JsonFile,$content);
    }

    /**
     * @return array|null
     */
    public function GetData()
    {
        return $this->Data;
    }

    /**
     * @param string $jsonFile
     */
    abstract protected function InitializeIfFileNotExists($jsonFile);
}
?>