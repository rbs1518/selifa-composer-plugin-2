<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer\Framework;
use Composer\Package\PackageInterface;
use Exception;

/**
 * Class InstallData
 *
 * @package RBS\Selifa\Composer\Framework
 */
class InstallData extends BaseDataObject
{
    /**
     * InstallData constructor.
     * @param Configuration $config
     */
    public function __construct(Configuration $config)
    {
        parent::__construct($config);
        $this->SetDataFileName('.selifa.json');
    }

    /**
     * @param string $jsonFile
     */
    protected function InitializeIfFileNotExists($jsonFile)
    {
        $this->Data = [
            'packages' => [],
            'ignores' => [
                '/vendor/',
                '/temps/'
            ]
        ];
    }

    /**
     * @param PackageInterface $pi
     * @throws Exception
     */
    public function AddPackageFromInterface(PackageInterface $pi)
    {
        $pName = $pi->getPrettyName();
        if (isset($this->Data['packages'][$pName]))
            unset($this->Data['packages'][$pName]);

        $distUrls = $pi->getDistUrls();
        if (count($distUrls) <= 0)
            throw new Exception('Unspecified dist url(s).');

        $this->Data['packages'][$pName] = array(
            'download_url' => $distUrls[0],
            'version' => $pi->getPrettyVersion(),
            'files' => [],
            'configurations' => []
        );
    }

    /**
     * @param string $pName
     * @param array $files
     * @param array $configs
     */
    public function AddPackage($pName,$files,$configs)
    {
        if (isset($this->Data['packages'][$pName]))
            unset($this->Data['packages'][$pName]);
        $this->Data['packages'][$pName] = array(
            'download_url' => '',
            'files' => $files,
            'configurations' => $configs
        );
    }

    /**
     * @param string $pName
     * @param string $url
     */
    public function SetPackageURL($pName,$url)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['download_url'] = trim($url);
    }

    /**
     * @param string $pName
     * @param string $file
     */
    public function AddPackageFile($pName,$file)
    {
        if (isset($this->Data['packages'][$pName]['files']))
            $this->Data['packages'][$pName]['files'][] = $file;
    }

    /**
     * @param string $pName
     * @param array $data
     */
    public function SetPackageConfiguration($pName,$data)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['configurations'] = $data;
    }

    /**
     * @param string $pName
     * @param string[] $data
     */
    public function SetCommandPackages($pName,$data)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['command-packages'] = $data;
    }

    /**
     * @param string $pName
     * @param string $key
     * @param array $data
     */
    public function AddConfigurationItem($pName,$key,$data)
    {
        if (isset($this->Data['packages'][$pName]['configurations']))
            $this->Data['packages'][$pName]['configurations'][$key] = $data;
    }

    /**
     * @param string $pName
     */
    public function ResetPackageFiles($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['files'] = [];
    }

    /**
     * @param string $pName
     */
    public function ResetPackageConfigurations($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['configurations'] = [];
    }

    /**
     * @param string $pName
     */
    public function ResetCommandPackages($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            $this->Data['packages'][$pName]['command-packages'] = [];
    }

    /**
     * @param $pName
     */
    public function RemovePackage($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            unset($this->Data['packages'][$pName]);
    }

    /**
     * @param string $pName
     * @return bool
     */
    public function IsPackageExists($pName)
    {
        return isset($this->Data['packages'][$pName]);
    }

    /**
     * @param string $pName
     * @return null|array
     */
    public function GetPackage($pName)
    {
        if (isset($this->Data['packages'][$pName]))
            return $this->Data['packages'][$pName];
        else
            return null;
    }

    /**
     * @return array
     */
    public function GetAllConfigurations()
    {
        $config = array();
        foreach ($this->Data['packages'] as $pName => $meta)
        {
            if (!isset($meta['configurations']))
                continue;

            $pConfig = $meta['configurations'];
            if (count($pConfig) > 0)
            {
                foreach ($pConfig as $key => $options)
                    $config[$key] = $options;
            }
        }
        return $config;
    }

    /**
     * @return string[]
     */
    public function GetAllCommandPackages()
    {
        $result = [];
        foreach ($this->Data['packages'] as $pName => $meta)
        {
            if (!isset($meta['command-packages']))
                continue;

            foreach ($meta['command-packages'] as $value)
                $result[] = $value;
        }
        return $result;
    }
}
?>