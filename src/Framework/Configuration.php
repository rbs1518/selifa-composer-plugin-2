<?php
namespace RBS\Selifa\Composer\Framework;

/**
 * Class Configuration
 *
 * @package RBS\Selifa\Composer\Framework
 */
class Configuration
{
    /**
     * @var null|Configuration
     */
    protected static $_Instance = null;

    /**
     * @var string
     */
    public $RootDir = '';

    /**
     * @var string
     */
    public $TempDir = '';

    /**
     * @var string
     */
    public $DevDir = '';

    /**
     * @return null|Configuration
     */
    public static function Initialize()
    {
        if (self::$_Instance == null)
            self::$_Instance = new Configuration();
        return self::$_Instance;
    }

    /**
     * @return Configuration
     */
    public static function Instance()
    {
        return self::$_Instance;
    }

    /**
     *
     */
    private function __construct()
    {
        $this->RootDir = (getcwd().DIRECTORY_SEPARATOR);
        $this->TempDir = ($this->RootDir.'temps'.DIRECTORY_SEPARATOR);
        $this->DevDir = ($this->RootDir.'dev.d'.DIRECTORY_SEPARATOR);
    }

    /**
     * @return array|mixed
     */
    public function LoadLocalPluginConfigFile()
    {
        $theFile = ($this->RootDir.'_plugin_config.php');
        if (file_exists($theFile))
            return include($theFile);
        else
            return [];
    }
}
?>