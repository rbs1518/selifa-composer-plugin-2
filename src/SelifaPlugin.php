<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Composer;
use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginEvents;
use Composer\Plugin\PluginInterface;
use RBS\Selifa\Composer\Command\CommandProvider;
use RBS\Selifa\Composer\Framework\Configuration;
use RBS\Selifa\Composer\Framework\InstallData;
use RBS\Selifa\Composer\Framework\LibraryInstaller;
use RBS\Selifa\Composer\IO\FileGenerator;
use RBS\Selifa\Composer\IO\FileManager;

/**
 * Class SelifaPlugin
 *
 * @package RBS\Selifa\Composer
 */
class SelifaPlugin implements PluginInterface, EventSubscriberInterface, Capable
{
    /**
     * @var null|IOInterface
     */
    private $_IO = null;

    /**
     * @var null|InstallData
     */
    private $_InstallData = null;

    /**
     * @var null|Configuration
     */
    private $_Config = null;

    #region PluginInterface implementation
    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->_IO = $io;
        $config = Configuration::Initialize();
        $this->_Config = $config;

        FileManager::Initialize($io,$config);
        FileGenerator::Initialize($io,$config);

        $this->_InstallData = new InstallData($config);
        $sInstaller = new LibraryInstaller($composer,$config,$io,
            $this->_InstallData,
            FileManager::Instance(),FileGenerator::Instance());
        $composer->getInstallationManager()
            ->addInstaller($sInstaller);
    }

    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {

    }

    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {

    }
    #endregion

    #region EventSubscriberInterface implementation
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            PluginEvents::INIT => 'OnInitEvent',
            'post-package-install' => 'OnPostPackageInstall',
            'post-autoload-dump' => 'OnAfterAutoloadDump',
        ];
    }
    #region

    #region Capable implementation
    /**
     * @return array
     */
    public function getCapabilities()
    {
        return [
            'Composer\Plugin\Capability\CommandProvider' => CommandProvider::class
        ];
    }
    #endregion

    /**
     *
     */
    public function OnInitEvent()
    {
        $this->_IO->write('Selifa Composer Plugin v2: <fg=cyan>installed</>');
    }

    /**
     *
     */
    public function OnPostPackageInstall()
    {
        //Perform installation clean up.
        $path  = ($this->_Config->TempDir.'composer-installs');
        if (file_exists($path))
            FileManager::Instance()->DeleteDirectory($path);
    }

    /**
     *
     */
    public function OnAfterAutoloadDump()
    {
        //$this->_IO->write('Selifa Composer Plugin v2: <fg=red>deactivated</>');
        $this->_InstallData->WriteToFile();

        //Perform installation clean up.
        $path  = ($this->_Config->TempDir.'composer-installs');
        if (file_exists($path))
            FileManager::Instance()->DeleteDirectory($path);
    }

    /**
     * @return null|InstallData
     */
    public function GetInstallData()
    {
        return $this->_InstallData;
    }
}
?>